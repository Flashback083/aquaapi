package de.waterdu.aquaapi.ui.internal;

import de.waterdu.aquaapi.ui.api.AquaUI;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class AquaUISlot extends Slot {
    public AquaUISlot(IInventory inventoryIn, int index, int xPosition, int yPosition) {
        super(inventoryIn, index, xPosition, yPosition);
    }

    @Override
    public int getSlotStackLimit() {
        return 1;
    }

    public boolean canTakeStack(EntityPlayer playerIn) {
        return false;
    }

    public boolean getHasStack()
    {
        return false;
    }

    public boolean isItemValid(ItemStack stack)
    {
        return false;
    }

    public void putStack(ItemStack stack) {
        AquaUIInventory i = (AquaUIInventory) inventory;
        AquaUI.openUI(i.player, i.page);
    }

    public ItemStack onTake(EntityPlayer thePlayer, ItemStack stack) {
        AquaUIInventory i = (AquaUIInventory) inventory;
        AquaUI.openUI(i.player, i.page);
        return ItemStack.EMPTY;
    }

    public void onSlotChange(ItemStack p_75220_1_, ItemStack p_75220_2_) {
        AquaUIInventory i = (AquaUIInventory) inventory;
        AquaUI.openUI(i.player, i.page);
    }
}
