package de.waterdu.aquaapi.ui.api;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;

import javax.annotation.Nullable;

/**
 * Class which make up what is shown on a {@link de.waterdu.aquaapi.ui.api.IPage}.
 * These can be created on the fly when a page is shown to a player, or can be created and held in advance.
 *
 * @author      Sam Plummer <sam@waterdu.de>
 * @version     1.0
 * @since       02/03/2019
 */
public class Button {
    /**
     * Name of this button. Shown as the first line of the button.
     */
    private String name;

    /**
     * Lore of this button. Shown as subsequent lines of the button.
     * Split by \n.
     */
    private String lore;

    /**
     * Item which this button is displayed as.
     */
    private Item item;

    /**
     * Index at which this button will be shown on the {@link de.waterdu.aquaapi.ui.api.IPage}.
     */
    private int index;

    /**
     * Reference to another {@link de.waterdu.aquaapi.ui.api.IPage} for easy pagination.
     */
    private IPage linkedPage;

    /**
     * Constructs a button with a linked page.
     *
     * @param name {@link de.waterdu.aquaapi.ui.api.Button#name}
     * @param lore {@link de.waterdu.aquaapi.ui.api.Button#lore}
     * @param item {@link de.waterdu.aquaapi.ui.api.Button#item}
     * @param index {@link de.waterdu.aquaapi.ui.api.Button#index}
     * @param linkedPage {@link de.waterdu.aquaapi.ui.api.Button#linkedPage}
     */
    public Button(String name, String lore, Item item, int index, IPage linkedPage) {
        this.name = name;
        this.lore = lore;
        this.item = item;
        this.index = index;
        this.linkedPage = linkedPage;
    }

    /**
     * Constructs a button without a linked page.
     *
     * @param name {@link de.waterdu.aquaapi.ui.api.Button#name}
     * @param lore {@link de.waterdu.aquaapi.ui.api.Button#lore}
     * @param item {@link de.waterdu.aquaapi.ui.api.Button#item}
     * @param index {@link de.waterdu.aquaapi.ui.api.Button#index}
     */
    public Button(String name, String lore, Item item, int index) {
        this.name = name;
        this.lore = lore;
        this.item = item;
        this.index = index;
        this.linkedPage = null;
    }

    /**
     * Creates the ItemStack which is then what is actually displayed in the UI as this button.
     *
     * @return ItemStack Display of this button.
     */
    public ItemStack getDisplay() {
        ItemStack stack = new ItemStack(this.item);
        stack.setStackDisplayName(this.name);
        if(this.lore != null) {
            NBTTagList lore = new NBTTagList();
            String[] loreLines = this.lore.split("\n");
            for(String loreLine : loreLines) {
                lore.appendTag(new NBTTagString(loreLine));
            }
            stack.getOrCreateSubCompound("display").setTag("Lore", lore);
        }
        return stack;
    }

    /**
     * {@link de.waterdu.aquaapi.ui.api.Button#linkedPage}.
     *
     * @return IPage Linked page of this button, or null.
     */
    @Nullable
    public IPage getLinkedPage() {
        return linkedPage;
    }

    /**
     * {@link de.waterdu.aquaapi.ui.api.Button#index}.
     *
     * @return int Index of this button.
     */
    public int getIndex() {
        return index;
    }
}
