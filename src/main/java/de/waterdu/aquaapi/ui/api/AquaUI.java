package de.waterdu.aquaapi.ui.api;

import de.waterdu.aquaapi.ui.internal.AquaUIContainer;
import de.waterdu.aquaapi.ui.internal.AquaUIInventory;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.play.server.SPacketOpenWindow;

import javax.annotation.Nullable;

/**
 * <h1>AquaUI</h1>
 * Chest UIs made in Forge directly.
 *
 * @author      Sam Plummer <sam@waterdu.de>
 * @version     1.0
 * @since       02/03/2019
 */
public class AquaUI {
    /**
     * Opens a UI page. All UI pages must be instances of a class implementing {@link de.waterdu.aquaapi.ui.api.IPage}.
     *
     * @param player The user to display the page to.
     * @param page The page to display. null closes any currently open page.
     * @return boolean True indicates success in showing page.
     */
    public static boolean openUI(EntityPlayerMP player, @Nullable IPage page) {
        boolean result = false;
        if(player != null) {
            result = true;
            player.closeContainer();
            player.closeScreen();
            if(page != null) {
                AquaUIInventory i = new AquaUIInventory(page, player);
                AquaUIContainer c = new AquaUIContainer(player.inventory, i, player);
                player.sendAllWindowProperties(c, i);
                player.sendContainerToPlayer(c);
                player.openContainer = c;
                player.connection.sendPacket(new SPacketOpenWindow(player.currentWindowId, "minecraft:container", i.getDisplayName(), page.getSize(player)));
                c.detectAndSendChanges();
                for (Button b : i.buttons) {
                    player.sendSlotContents(player.openContainer, b.getIndex(), b.getDisplay());
                }
            }
        }
        return result;
    }
}
