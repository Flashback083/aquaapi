package de.waterdu.aquaapi;

import net.minecraftforge.fml.common.Mod;

/**
 * <h1>AquaAPI</h1>
 * Collection of useful tools for plugin developers.
 * Designed to be shaded in, or copied from directly. Can also be used as a library.
 *
 * AquaAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AquaAPI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with AquaAPI. If not, see <https://www.gnu.org/licenses/lgpl.txt>.
 *
 * @author      Sam Plummer <sam@waterdu.de>
 * @version     1.0
 * @since       02/03/2019
 */
@Mod(name = AquaAPI.MODNAME, version = AquaAPI.MODVERSION, acceptableRemoteVersions = "*", modid = AquaAPI.MODID)
public class AquaAPI {
    public static final String MODVERSION = "1.12.2-1.0.0-server";
    public static final String MODID = "aquaapi";
    public static final String MODNAME = "AquaAPI";

    @Mod.Instance(MODID)
    public static AquaAPI instance;
}